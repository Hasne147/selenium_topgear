import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment_1 {
	
	String email;
	String password;
	WebDriver driver1;
	
	void readFile()
	{
		try {
			File src=new File("D:\\Selenium_softwares\\Files\\Login.xls");
			FileInputStream fis=new FileInputStream(src);
			XSSFWorkbook wb=new XSSFWorkbook(fis);
			XSSFSheet sheet=wb.getSheetAt(0);
			 email=sheet.getRow(0).getCell(0).getStringCellValue();
			 password=sheet.getRow(1).getCell(0).getStringCellValue();	
			 System.out.println(email);
			 System.out.println(password);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	void set_up_browser()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_softwares\\chromedriver_win32\\chromedriver.exe");
		driver1=new ChromeDriver();
		driver1.manage().window().maximize();
		driver1.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver1.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	}
	
	void log_in() throws InterruptedException
	{
		driver1.get("https://demo.opencart.com/");
		driver1.findElement(By.linkText("My Account")).click();
		Thread.sleep(2000);
		driver1.findElement(By.linkText("Login")).click();
		Thread.sleep(3000);
		driver1.findElement(By.id("input-email")).sendKeys(email);
		driver1.findElement(By.id("input-password")).sendKeys(password);
		Thread.sleep(2000);
		driver1.findElement(By.cssSelector("input[class='btn btn-primary']")).click();
		Thread.sleep(3000);
	}
	
	void log_out() throws InterruptedException
	{
		driver1.findElement(By.linkText("My Account")).click();
		Thread.sleep(3000);
		driver1.findElement(By.linkText("Logout")).click();
		
	}
	
	void close_browser()
	{
		driver1.close();
		driver1.quit();
	}

	public static void main(String[] args) throws InterruptedException {
		Assignment_1 obj=new Assignment_1();
		obj.readFile();
		obj.set_up_browser();
		obj.log_in();
		obj.log_out();

	}

}
