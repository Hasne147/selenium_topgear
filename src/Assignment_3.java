import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Assignment_3 {
	
	WebDriver driver1;
	
	void set_up_browser()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_softwares\\chromedriver_win32\\chromedriver.exe");
		driver1=new ChromeDriver();
		driver1.manage().window().maximize();
		driver1.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver1.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	}
	
	void log_in() throws InterruptedException
	{
		driver1.get("https://demo.opencart.com/");
		driver1.findElement(By.linkText("My Account")).click();
		Thread.sleep(2000);
		driver1.findElement(By.linkText("Login")).click();
		Thread.sleep(3000);
		driver1.findElement(By.id("input-email")).sendKeys("sneha147@gmail.com");
		driver1.findElement(By.id("input-password")).sendKeys("12345");
		Thread.sleep(2000);
		driver1.findElement(By.cssSelector("input[class='btn btn-primary']")).click();
		Thread.sleep(3000);
	}
	
	void search_product() throws InterruptedException
	{
		driver1.findElement(By.name("search")).sendKeys("Components");
		driver1.findElement(By.name("search")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		
		Select select=new Select(driver1.findElement(By.name("category_id")));
		select.selectByValue("28");
		Thread.sleep(3000);
		
		driver1.findElement(By.name("sub_category")).click();
		driver1.findElement(By.id("button-search")).click();
		
		Thread.sleep(3000);
		
	}
	
	
	void sort_and_compare() throws InterruptedException
	{
		driver1.findElement(By.linkText("Phones & PDAs")).click();
		Thread.sleep(2000);
		
		Select sort=new Select(driver1.findElement(By.id("input-sort")));
		sort.selectByVisibleText("Price (High > Low)");
		
		driver1.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div[1]/div/div[2]/div[2]/button[3]")).click();
		driver1.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div[2]/div/div[2]/div[2]/button[3]")).click();
		driver1.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div[3]/div/div[2]/div[2]/button[3]")).click();
		Thread.sleep(3000);
		
		driver1.findElement(By.id("compare-total")).click();
		Thread.sleep(2000);
	}
	
	
	void add_to_cart() throws InterruptedException
	{
		driver1.findElement(By.linkText("HTC Touch HD")).click();
		driver1.findElement(By.id("button-cart")).click();
		Thread.sleep(2000);
		driver1.findElement(By.linkText("shopping cart")).click();
		Thread.sleep(2000);
	}
	
	void check_out()
	{
		driver1.findElement(By.linkText("Checkout")).click();
	}
	
	void log_out() throws InterruptedException
	{
		driver1.findElement(By.linkText("My Account")).click();
		Thread.sleep(3000);
		driver1.findElement(By.linkText("Logout")).click();
		
	}
	

	public static void main(String[] args) throws InterruptedException {
		
		Assignment_3 obj=new Assignment_3();
		
		obj.set_up_browser();
		obj.log_in();
		obj.search_product();
		obj.sort_and_compare();
		obj.add_to_cart();
		obj.log_out();
	

	}

}
