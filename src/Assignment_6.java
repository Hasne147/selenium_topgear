import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Assignment_6 {
	
	static WebDriver driver1;
	
	@BeforeClass
	public static void set_up_browser()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_softwares\\chromedriver_win32\\chromedriver.exe");
		driver1=new ChromeDriver();
		driver1.manage().window().maximize();
		driver1.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver1.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	}
	
	@Before
	public void log_in() throws InterruptedException
	{
		driver1.get("https://demo.opencart.com/");
		driver1.findElement(By.linkText("My Account")).click();
		Thread.sleep(2000);
		driver1.findElement(By.linkText("Login")).click();
		Thread.sleep(3000);
		driver1.findElement(By.id("input-email")).sendKeys("sneha147@gmail.com");
		driver1.findElement(By.id("input-password")).sendKeys("12345");
		Thread.sleep(2000);
		driver1.findElement(By.cssSelector("input[class='btn btn-primary']")).click();
		Thread.sleep(3000);
	}
	
	@Test
	public void search_product() throws InterruptedException
	{
		driver1.findElement(By.name("search")).sendKeys("canon");
		driver1.findElement(By.name("search")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		
		driver1.findElement(By.cssSelector("img[alt='Canon EOS 5D'][title='Canon EOS 5D']")).click();
		
			
		
	}
	
	@After
	public void log_out() throws InterruptedException
	{
		driver1.findElement(By.linkText("My Account")).click();
		Thread.sleep(3000);
		driver1.findElement(By.linkText("Logout")).click();
		
	}
	
	@AfterClass
	public static void close_browser()
	{
		driver1.close();
		driver1.quit();
	}
	
	
	
	

}
